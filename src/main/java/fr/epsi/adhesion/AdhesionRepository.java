package fr.epsi.adhesion;

import fr.epsi.api.APIAdhesion;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class AdhesionRepository {

    @Resource(name = "adherentDataSource")
	private DataSource dataSource;
	
	public void create(Adhesion adhesion, String confirmationMotDePasse) throws SQLException, ValidationException {
		adhesion.valider(confirmationMotDePasse);
		
		String sql = "insert into Adherent (email, motDePasse, dateAdhesion) values (?, SHA1(?), ?)";
		try (Connection connection = dataSource.getConnection();
		     PreparedStatement ptstmt = connection.prepareStatement(sql)) {
			ptstmt.setString(1, adhesion.getEmail());
			ptstmt.setString(2, adhesion.getMotDePasse());
			ptstmt.setDate(3, new Date(adhesion.getDateAdhesion().getTime()));
			
			ptstmt.executeUpdate();
		}
	}

	public void create(APIAdhesion jsonAdhesion) throws SQLException, ValidationException {
	    Adhesion adhesion = new Adhesion();
	    adhesion.setEmail(jsonAdhesion.getEmail());
	    adhesion.setMotDePasse(jsonAdhesion.getMotDePasse());
	    adhesion.setConditionsAcceptees(true);
	    create(adhesion, adhesion.getMotDePasse());
    }
	
	public List<Adhesion> getAll() throws SQLException {
		List<Adhesion> adhesions = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
		     Statement stmt = connection.createStatement();
		     ResultSet rs = stmt.executeQuery("select email, motDePasse, dateAdhesion from Adherent order by dateAdhesion")) {
			while (rs.next()) {
				Adhesion adhesion = new Adhesion();
				adhesion.setEmail(rs.getString("email"));
				adhesion.setDateAdhesion(rs.getDate("dateAdhesion"));
				adhesions.add(adhesion);
			}
		}
		return adhesions;
	}

	public void delete(APIAdhesion adhesion) throws SQLException {
		String sql = "delete from Adherent where email like ?";
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement ptstmt = connection.prepareStatement(sql)) {
			ptstmt.setString(1, adhesion.getEmail());
			ptstmt.executeUpdate();
		}
	}

}
